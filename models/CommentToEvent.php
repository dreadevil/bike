<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CommentsToEvents".
 *
 * @property integer $id_comment_to_event
 * @property integer $fid_comment
 * @property integer $fid_event
 *
 * @property Comments $fidComment
 * @property Events $fidEvent
 */
class CommentToEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CommentsToEvents';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_comment_to_event', 'fid_comment', 'fid_event'], 'required'],
            [['id_comment_to_event', 'fid_comment', 'fid_event'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_comment_to_event' => Yii::t('app', 'Id Comment To Event'),
            'fid_comment' => Yii::t('app', 'Fid Comment'),
            'fid_event' => Yii::t('app', 'Fid Event'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFidComment()
    {
        return $this->hasOne(Comments::className(), ['id_comment' => 'fid_comment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFidEvent()
    {
        return $this->hasOne(Events::className(), ['id_event' => 'fid_event']);
    }
}
