<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "images".
 *
 * @property integer $id_image
 * @property string $link
 * @property integer $fid_album
 */
class Image extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link', 'fid_album'], 'required'],
            [['fid_album'], 'integer'],
            [['link'], 'string', 'max' => 150],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_image' => Yii::t('app', 'Id Image'),
            'link' => Yii::t('app', 'Link'),
            'fid_album' => Yii::t('app', 'Fid Album'),
        ];
    }

    public function getAlbum()
    {
        return $this->hasOne(ImagesAlbum::className(), ['id_album' => 'fid_album']);
    }

    public function getAlbums()
    {
        return ArrayHelper::map(ImagesAlbum::find()->all(), 'id_album', 'name');
    }
}
