<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "imagesalbums".
 *
 * @property integer $id_album
 * @property string $name
 */
class ImagesAlbum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'imagesalbums';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_album' => Yii::t('app', 'Id Album'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['fid_album' => 'id_album']);
    }
}
