<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "CommentsToNews".
 *
 * @property integer $id_comment_to_news
 * @property integer $fid_comment
 * @property integer $fid_news
 *
 * @property News $fidNews
 * @property Comments $fidComment
 */
class CommentToNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'CommentsToNews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_comment_to_news', 'fid_comment', 'fid_news'], 'required'],
            [['id_comment_to_news', 'fid_comment', 'fid_news'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_comment_to_news' => Yii::t('app', 'Id Comment To News'),
            'fid_comment' => Yii::t('app', 'Fid Comment'),
            'fid_news' => Yii::t('app', 'Fid News'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFidNews()
    {
        return $this->hasOne(News::className(), ['id_news' => 'fid_news']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFidComment()
    {
        return $this->hasOne(Comments::className(), ['id_comment' => 'fid_comment']);
    }
}
