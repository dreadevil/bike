<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\behaviors\DateTimestampBehavior;

/**
 * This is the model class for table "News".
 *
 * @property integer $id_news
 * @property string $title
 * @property string $description
 * @property integer $is_in_slider
 * @property string $slider_image
 * @property string $youtube_link
 * @property string $created_at
 * @property string $updated_at
 *
 * @property CommentsToNews[] $commentsToNews
 */
class News extends ActiveRecord
{
    public $sliderFile;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'News';
    }

    public function behaviors()
    {
        return [
            'dateTimestampBehavior' => [
                'class' => DateTimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sliderFile'], 'file', 'extensions' => 'png, jpg', 'checkExtensionByMimeType' => false],
            [['title'], 'required'],
            [['is_in_slider'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['slider_image'], 'string', 'max' => 150],
            [['title', 'youtube_link'], 'string', 'max' => 45],
            [['description'], 'string', 'max' => 1500]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_news' => Yii::t('app', 'Id News'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'is_in_slider' => Yii::t('app', 'Is In Slider'),
            'slider_image' => Yii::t('app', 'Slider Image'),
            'youtube_link' => Yii::t('app', 'Youtube Link'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['id_comment' => 'fid_comment'])
            ->viaTable('CommentsToNews', ['fid_news' => 'id_news']);
    }
}
