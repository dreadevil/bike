<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use app\behaviors\DateTimestampBehavior;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id_comment
 * @property string $content
 * @property string $author
 * @property string $email
 * @property string $created_at
 *
 * @property CommentsToEvents[] $commentsToEvents
 * @property CommentsToNews[] $commentsToNews
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    public function behaviors()
    {
        return [
            'dateTimestampBehavior' => [
                'class' => DateTimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content', 'author'], 'required'],
            [['created_at'], 'safe'],
            [['content'], 'string', 'max' => 200],
            [['author'], 'string', 'max' => 45],
            [['email'], 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_comment' => Yii::t('app', 'Id Comment'),
            'content' => Yii::t('app', 'Content'),
            'author' => Yii::t('app', 'Author'),
            'email' => Yii::t('app', 'Email'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id_event' => 'fid_event'])
            ->viaTable('CommentsToEvents', ['fid_comment' => 'id_comment']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::className(), ['id_news' => 'fid_news'])
            ->viaTable('CommentsToNews', ['fid_comment' => 'id_comment']);
    }
}
