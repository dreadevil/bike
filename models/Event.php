<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Events".
 *
 * @property integer $id_event
 * @property string $name
 * @property string $description
 * @property string $place
 * @property integer $status
 * @property string $start_date
 * @property string $finish_date
 *
 * @property CommentsToEvents[] $commentsToEvents
 */
class Event extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Events';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'place', 'start_date', 'finish_date'], 'required'],
            [['status'], 'integer'],
            [['start_date', 'finish_date'], 'safe'],
            [['name', 'description', 'place'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_event' => Yii::t('app', 'Id Event'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'place' => Yii::t('app', 'Place'),
            'status' => Yii::t('app', 'Status'),
            'start_date' => Yii::t('app', 'Start Date'),
            'finish_date' => Yii::t('app', 'Finish Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['id_comment' => 'fid_comment'])
            ->viaTable('CommentsToEvents', ['fid_event' => 'id_event']);
    }
}
