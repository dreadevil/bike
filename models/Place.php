<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "Places".
 *
 * @property integer $id_place
 * @property string $name
 * @property string $description
 * @property double $lat_coordinate
 * @property double $len_coordinate
 * @property string $category
 */
class Place extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Places';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'lat_coordinate', 'len_coordinate', 'category'], 'required'],
            [['lat_coordinate', 'len_coordinate'], 'number'],
            [['name', 'description', 'category'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_place' => Yii::t('app', 'Id Place'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'lat_coordinate' => Yii::t('app', 'Lat Coordinate'),
            'len_coordinate' => Yii::t('app', 'Len Coordinate'),
            'category' => Yii::t('app', 'Category'),
        ];
    }
}
