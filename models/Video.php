<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "video".
 *
 * @property integer $id_video
 * @property string $link
 * @property integer $type
 */
class Video extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['link', 'type'], 'required'],
            [['type'], 'integer'],
            [['link'], 'string', 'max' => 200],
            [['type'], 'unique'],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_video' => Yii::t('app', 'Id Video'),
            'link' => Yii::t('app', 'Link'),
            'type' => Yii::t('app', 'Type'),
        ];
    }
}
