<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 13.02.15
 * Time: 18:02
 */
use yii\helpers\Html;

$this->title = Yii::t('app', 'Video');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach ($video as $item): ?>
        <div class="clearfix">
            <?php if ($item->type === 3): ?>
                <video controls="controls">
                    <source src="<?= $item->link ?>">
                </video>
            <?php else: ?>
                <?= $item->link ?>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
</div>