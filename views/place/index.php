<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 13.02.15
 * Time: 18:02
 */
use yii\helpers\Html;

$this->title = Yii::t('app', 'Places');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="place-index">

    <h1><?= Html::encode($this->title) ?></h1>

</div>