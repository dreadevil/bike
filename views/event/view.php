<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 16.02.15
 * Time: 13:40
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-view">
    <section>
        <h1>
            <?= Html::encode($event->name) ?>
        </h1>
        <article>
            <?= $event->description ?>
        </article>
    </section>
</div>
<div class="event-comments">
    <?php foreach ($event->comments as $item): ?>
        <div class="event-comments__author">
            <?= Yii::t('app', 'Author') . ':' . Html::a($item->author, "mailto:$item->email") ?>
            <span class="pull-right">
                <?= Html::encode($item->created_at) ?>
            </span>
        </div>
        <p class="event-comments__content">
            <?= Html::encode($item->content) ?>
        </p>
    <?php endforeach; ?>
    <div class="event-comments__form">
        <?php $form = ActiveForm::begin([
            'action' => \yii\helpers\Url::to('/comment/create'),
        ]) ?>
        <?= Html::hiddenInput('event', $event->id_event) ?>
        <?= $form->field($comment, 'author')->textInput() ?>
        <?= $form->field($comment, 'email')->input('email') ?>
        <?= $form->field($comment, 'content')->textarea(['rows' => 5]) ?>
        <?= Html::submitButton(Yii::t('app', 'Add comment'), ['class' => 'btn btn-default']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
