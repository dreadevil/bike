<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 13.02.15
 * Time: 18:02
 */
use yii\helpers\Html;

$this->title = Yii::t('app', 'Events');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach ($events as $item): ?>
        <section class="clearfix">
            <h1>
                <?= Html::encode($item->name) ?>
            </h1>
            <article>
                <?= $item->description ?>
            </article>
            <span>
                <?= Html::a(Yii::t('app', 'More'), ['event/view', 'id' => $item->id_event], ['class' => 'event-link']) ?>
            </span>
        </section>
    <?php endforeach; ?>
</div>