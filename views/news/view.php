<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 16.02.15
 * Time: 13:40
 */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view clearfix">
    <section>
        <h1>
            <?= Html::encode($news->title) ?>
        </h1>
        <article>
            <?= $news->description ?>
        </article>
    </section>
</div>
<div class="news-comments">
    <?php foreach ($news->comments as $item): ?>
        <div class="news-comments__author">
            <?= Yii::t('app', 'Author') . ':' . Html::a($item->author, "mailto:$item->email") ?>
            <span class="pull-right">
                <?= Html::encode($item->created_at) ?>
            </span>
        </div>
        <p class="news-comments__content">
            <?= Html::encode($item->content) ?>
        </p>
    <?php endforeach; ?>
    <div class="news-comments__form">
        <?php $form = ActiveForm::begin([
            'action' => \yii\helpers\Url::to('/comment/create'),
        ]) ?>
        <?= Html::hiddenInput('news', $news->id_news) ?>
        <?= $form->field($comment, 'author')->textInput() ?>
        <?= $form->field($comment, 'email')->input('email') ?>
        <?= $form->field($comment, 'content')->textarea(['rows' => 5]) ?>
        <?= Html::submitButton(Yii::t('app', 'Add comment'), ['class' => 'btn btn-default']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
