<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 13.02.15
 * Time: 18:02
 */
use yii\helpers\Html;

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php foreach($newses as $item): ?>
        <section class="clearfix">
            <h1>
                <?= Html::encode($item->title) ?>
            </h1>
            <article>
                <?= $item->description ?>
            </article>
            <span>
                <?= Html::a(Yii::t('app', 'More'), ['news/view', 'id' => $item->id_news], ['class' => 'news-link']) ?>
            </span>
        </section>
    <?php endforeach; ?>
</div>