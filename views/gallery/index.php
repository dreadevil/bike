<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 13.02.15
 * Time: 18:02
 */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Gallery');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul>
        <?php foreach ($albums as $item): ?>
            <li>
                <?= Html::a($item->name,Url::to(['gallery/view', 'id' => $item->id_album])) ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>