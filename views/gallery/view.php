<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 13.02.15
 * Time: 18:02
 */
use yii\helpers\Html;

$this->title = $album->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gallery'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-view">

    <h1><?= Html::encode($album->name) ?></h1>


    <ul>
        <?php foreach ($album->images as $item): ?>
            <li>
                <?= Html::img($item->link, ['width' => '50%']) ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>