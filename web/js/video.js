/**
 * Created by bw on 18.02.15.
 */
var select = document.getElementById('video-type'),
    fileInputDiv = document.getElementsByClassName('field-video-file')[0],
    fileInput = document.getElementById('video-file');
select.addEventListener('change', function () {
    if (this.value == 3) {
        fileInputDiv.classList.remove('hidden');
        fileInput.removeAttribute('disabled');
    }
    else {
        fileInputDiv.classList.add('hidden');
        fileInput.setAttribute('disabled', '');
    }
});