/**
 * Created by bw on 16.02.15.
 */

var checkbox = document.getElementById('news-is_in_slider'),
    slider_image_input = document.getElementById('news-sliderfile');
document.addEventListener('DOMContentLoaded', function () {
    if (checkbox.checked) {
        slider_image_input.removeAttribute('disabled');
    }
});
checkbox.addEventListener('change', function(){
    if (this.checked) {
        slider_image_input.removeAttribute('disabled');
    }
    else {
        slider_image_input.setAttribute('disabled', '');
    }
});
