<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 17.02.15
 * Time: 17:24
 */

namespace app\controllers;

use app\models\Event;
use Yii;
use yii\web\Controller;
use app\models\Comment;
use app\models\News;

class CommentController extends Controller
{
    public function actionCreate()
    {
        $model = new Comment();
        if (Yii::$app->request->isPost && $model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                if (isset($_POST['news'])) {
                    $news = News::findOne($_POST['news']);
                    $model->link('news', $news);
                }
                if (isset($_POST['event'])) {
                    $event = Event::findOne($_POST['event']);
                    $model->link('event', $event);
                }
            }
        }
        return $this->goBack();
    }

}