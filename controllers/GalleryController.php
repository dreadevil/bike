<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 20.02.2015
 * Time: 14:02
 */

namespace app\controllers;

use app\models\ImagesAlbum;
use Yii;
use yii\web\Controller;


class GalleryController extends Controller
{
    public function actionIndex()
    {
        $albums = ImagesAlbum::find()->all();
        return $this->render('index', ['albums' => $albums]);
    }

    public function actionView($id)
    {
        $album = ImagesAlbum::find()->where(['id_album' => $id])->with('images')->one();
        return $this->render('view', ['album' => $album]);
    }

}