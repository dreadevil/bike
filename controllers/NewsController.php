<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 13.02.15
 * Time: 17:57
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\News;
use app\models\Comment;


class NewsController extends Controller
{
    public function actionIndex()
    {
        $newses = News::find()->all();
        return $this->render('index', ['newses' => $newses]);
    }

    public function actionView($id)
    {
        $news = News::findOne(['id_news' => $id]);
        $comment = new Comment();
        return $this->render('view', [
            'news' => $news,
            'comment' => $comment,
        ]);
    }

}