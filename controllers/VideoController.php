<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 18.02.15
 * Time: 17:08
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Video;

class VideoController extends Controller
{
    public function actionIndex()
    {
        $video = Video::find()->all();
        return $this->render('index', ['video' => $video]);
    }

}