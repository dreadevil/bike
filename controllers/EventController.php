<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 16.02.15
 * Time: 14:19
 */

namespace app\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use app\models\Event;
use app\models\Comment;

class EventController extends Controller
{
    public function actionIndex()
    {
        $events = Event::find()->all();
        return $this->render('index', ['events' => $events]);
    }

    public function actionView($id)
    {
        $event = Event::findOne(['id_event' => $id]);
        $comment = new Comment();
        return $this->render('view', [
            'event' => $event,
            'comment' => $comment,
        ]);
    }

    public function actionList($month, $year)
    {
        $events = Event::find()
            ->where('start_date like :yearmonth', [':yearmonth' => $year.'-'.$month.'%'])
            ->all();
        echo Json::encode($events);
        die;
    }
}