<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 16.02.15
 * Time: 15:10
 */

namespace app\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Controller;
use app\models\Place;

class PlaceController extends Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionList()
    {
        $places = Place::find()->all();
        echo Json::encode($places);
        die;
    }

}