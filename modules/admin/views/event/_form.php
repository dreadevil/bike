<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Event */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="event-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'basic',
    ]) ?>

    <?= $form->field($model, 'place')->textInput(['maxlength' => 45]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= $form->field($model, 'start_date')->widget(DateTimePicker::className(), [
        'language' => 'ru',
            'size' => 'ms',
            'template' => '{input}',
            'pickButtonIcon' => 'glyphicon glyphicon-time',
            'clientOptions' => [
                'startView' => 3,
                'minView' => 0,
                'maxView' => 3,
                'weekStart' => 1,
                'autoclose' => true,
                'todayButton' => true,
            ]
    ]) ?>

    <?= $form->field($model, 'finish_date')->widget(DateTimePicker::className(), [
        'language' => 'ru',
            'size' => 'ms',
            'template' => '{input}',
            'pickButtonIcon' => 'glyphicon glyphicon-time',
            'clientOptions' => [
                'startView' => 3,
                'minView' => 0,
                'maxView' => 3,
                'weekStart' => 1,
                'autoclose' => true,
                'todayButton' => true,
            ]
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
