<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ImagesAlbum */

$this->title = Yii::t('app', 'Create {modelClass}', [
    'modelClass' => 'Images Album',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Images Albums'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="images-album-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
