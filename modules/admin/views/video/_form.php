<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJsFile('/js/video.js');

/* @var $this yii\web\View */
/* @var $model app\models\Video */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'link')->textInput(['maxlength' => 200]) ?>

    <?= $form->field($model, 'type')->listBox([1 => 'vk', 'youtube', Yii::t('app', 'Upload')], ['size' => 1]) ?>

    <?= $form->field($model, 'file', ['options' => ['class' => 'hidden']])->fileInput(['disabled' => '']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
