<?php
/**
 * Created by PhpStorm.
 * User: bw
 * Date: 16.02.15
 * Time: 16:14
 */
return [
    'Home' => 'Главная',
    'News' => 'Новости',
    'Events' => 'События',
    'Places' => 'Места',
    'Login' => 'Войти',
    'Logout ({username})' => 'Выйти ({username})',
    'Create {modelClass}' => 'Создать {modelClass}',
    'Update' => 'Обновить',
    'Delete' => 'Удалить',
    'Title' => 'Название',
    'Description' => 'Описание',
    'Is In Slider' => 'Показывать в слайдере',
    'Slider Image' => 'Изображение в слайдере',
    'Youtube Link' => 'Ссылка на Youtube',
    'Created At' => 'Создано',
    'Updated At' => 'Обновлено',
    'Name' => 'Имя',
    'More' => 'Подробнее',
    'Video' => 'Видео',
    'Lat Coordinate' => 'Широта (lat)',
    'Len Coordinate' => 'Долгота (lon)',
    'Images Albums' => 'Альбомы изображений',
    'Images' => 'Изображения',
    'Category' => 'Категория',
    'Place' => 'Место',
    'Gallery' => 'Галерея',
    'Status' => 'Завершено',
    'Start Date' => 'Дата начала',
    'Finish Date' => 'Дата окончания',
];